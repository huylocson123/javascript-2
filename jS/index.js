/* 
input
    - số ngày 
    - tiền lương 1 ngày
process
    - tao bien day
    - tao bien luong
    - tao bien tongluong
    - tong luong = day * luong
out put
    - tongluong
*/
function salary() {
    var oneDay = document.getElementById("one-day").value * 1,
        day = document.getElementById("day").value * 1;
    var sumSa = oneDay * day;
    document.getElementById("tien-luong").value = sumSa + " VND";
}

/* 
input
-  5 số thực

process 
- tạo 5 biến num1 num2 num3 num4 num5
- tạo biến kq
- tổng 5 số thực / 5
out put
-giá trị trung bình
*/

function sum() {
    var num1 = document.getElementById("num1").value * 1,
        num2 = document.getElementById("num2").value * 1,
        num3 = document.getElementById("num3").value * 1,
        num4 = document.getElementById("num4").value * 1,
        num5 = document.getElementById("num5").value * 1;
    var avg = (num1 + num2 + num3 + num4 + num5) / 5;
    document.getElementById("avg").value = avg;
}
/* 
input
    - số tiền
    - vnd = 245000

process
    - tạo biến USD
    - tạo biến VND = 245000
    -tao biến giá tri = sotien USD * 245000
out put
    - giá trị quy đổi
*/
function exchange() {
    var vnd = document.getElementById("vnd").value * 1,
        usd = document.getElementById("usd").value * 1;
    var resultEx = vnd * usd;
    document.getElementById("quy-doi").value = resultEx + " VND";

}
/* 
input
    - chiều dài
    - chiều rộng 
    - diện tích 
    -  chu vi
process
    -tạo biến dai
    -tạo biến rong
    -tạo biến S
    -tạo biến V
    S = dai * rong 
    V = (dai + rong )* 2
out put
    S, V
*/
function square() {
    var long = document.getElementById("long").value * 1,
        short = document.getElementById("short").value * 1,
        acreage = long * short,
        perimeter = (long + short) * 2;
    document.getElementById("perimeter").value = perimeter;
    document.getElementById("acreage").value = acreage;
}
/* 
input
    -1 số có 2 chữ số
process
    - tạo biến so
    - tao bien so nguyen hangDv
    - tao bien so nguyen hangC
    - handDv = so % 10
    - handC = so / 10
    - tao bien kq =hangDv + hangC
out put
    - in ra màn hình tổng 2 kí số 
*/
function variable() {
    var num = document.getElementById("chu-so").value * 1,
        hangDv = parseInt(num % 10),
        hangC = parseInt(num / 10),
        kq = hangDv + hangC;
    document.getElementById("sum-num").value = kq;
}